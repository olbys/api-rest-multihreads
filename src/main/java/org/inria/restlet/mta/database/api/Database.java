package org.inria.restlet.mta.database.api;

import java.util.Collection;
import java.util.List;

import org.inria.restlet.mta.backend.Buffet;
import org.inria.restlet.mta.backend.Client;


/**
 *
 * Interface to the database.
 *
 * @author msimonin
 * @author Fabrice TRA
 * @author Jean OLivier KOKO
 *
 */
public interface Database
{

    /**
     * Returne la liste des Clients du restaurant
     * @return Collection <{@link Client}
     */
    Collection<Client> getClients();

    /**
     *  Returne l'utilisateur avec l'id donné
     * @param id
     * @return Client
     */
    Client getClient(int id);


    /**
     *
     * @return Retourne l'état du buffet
     */
    Buffet getBuffet();

}