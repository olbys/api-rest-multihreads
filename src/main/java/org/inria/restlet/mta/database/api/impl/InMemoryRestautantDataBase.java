/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inria.restlet.mta.database.api.impl;

import org.inria.restlet.mta.backend.*;
import org.inria.restlet.mta.database.api.Database;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Jean Olivier Koko
 * @author Fabrice TRA
 */
public class InMemoryRestautantDataBase implements Database {

    private final static int LIMITED_CLIENT = 25;
    private ArrayList<Client> clientArrayList;
    private int nombre_client = 0;
    private Buffet buffet;
    private EmployeeBuffet employeeBuffet;
    private Cuisinier cuisinier;
    private Stand_cuisson stand_cuisson;

    /**
     * methode à appeler par un client lorsqu'il veut rentrer le restaurant
     * @throws InterruptedException
     */
    synchronized public void Entrer_restaurant() throws InterruptedException {
        while (nombre_client == LIMITED_CLIENT) {
            this.wait();
        }
        augmenterClient();
    }

    /**
     *  methode à appeler par un client lorsqu'il veut sortir du restaurant
     * @throws InterruptedException
     */
    synchronized public void Sortir_restaurant() throws InterruptedException {
        diminuerClient();
        this.notifyAll();
    }

    /**
     * Lorsqu'il y a de la place et qu'un client reussit à rentrer on augmente le nombre de cient present a l'instant dans le restaurant
     */
    synchronized public void augmenterClient() {
        this.nombre_client++;
    }

    /**
     * Lorsqu'un qu'un client reussit à sortir du resto on diminue le nombre de cient present a l'instant dans le restaurant
     */
    synchronized public void diminuerClient() {
        this.nombre_client--;
    }

    /**
     * retourne le buffet du restaurant
     * @return Buffet
     */
    @Override
    public Buffet getBuffet() {
        return buffet;
    }

    /**
     * @param buffet {@link org.inria.restlet.mta.backend.Buffet}
     */
    public void setBuffet(Buffet buffet) {
        this.buffet = buffet;
    }

    /**
     * retourne l'etat du stand de cuisson du restaurant
     *
     * @return Stand_cuisson {@link org.inria.restlet.mta.backend.Stand_cuisson}
     */
    public Stand_cuisson getStand_cuisson() {
        return stand_cuisson;
    }

    /**
     * @param stand_cuisson {@link org.inria.restlet.mta.backend.Stand_cuisson}
     */
    public void setStand_cuisson(Stand_cuisson stand_cuisson) {
        this.stand_cuisson = stand_cuisson;
    }


    @Override
    public Collection<Client> getClients() {
        return this.clientArrayList;
    }

    @Override
    public Client getClient(int id) {
        if(this.clientArrayList.size() < id){
            return null;
        }
        return this.clientArrayList.get(id);
    }


    /**
     * Constructor
     */
    public InMemoryRestautantDataBase() {

        //instanciation du buffet
        this.buffet = new Buffet();

        //instanciation de l'employe de buffet
        this.employeeBuffet = new EmployeeBuffet(buffet);
        this.employeeBuffet.start();

        //instanciation du stand de cuisson
        this.stand_cuisson = new Stand_cuisson();

        //instanciation du cuisinier
        this.cuisinier = new Cuisinier(stand_cuisson);
        this.cuisinier.start();

        //instanciation de la liste des clients
        this.clientArrayList = new ArrayList<Client>();

        //instanciation des clients
        for (int i = 0; i < 100; i++) {
            clientArrayList.add(new Client(i , this));
            clientArrayList.get(i).start();
        }


    }

    
    
}
