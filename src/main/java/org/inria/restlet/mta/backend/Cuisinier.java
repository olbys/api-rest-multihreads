package org.inria.restlet.mta.backend;

public class Cuisinier extends Thread {
    private Stand_cuisson stand;

    /**
     * Constructeur de la classe cuisinier
     * @param stand {@link Stand_cuisson}
     */
    public Cuisinier(Stand_cuisson stand) {
        this.stand = stand;
        this.setDaemon(true);

    }

    /**
     * Routine du cuisinier : Il veille à cuire les repas des clients
     */
    private void Commencer_service(){
        System.out.println("le cuisinier commence son service");
        while (true){
            try {
                this.stand.Cuire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Thread du cuisinier
     */
    @Override
    public void run() {
        Commencer_service();
	}

}