package org.inria.restlet.mta.backend;

import org.inria.restlet.mta.database.api.Database;
import org.inria.restlet.mta.database.api.impl.InMemoryRestautantDataBase;

/**
 *
 * Backend for all resources.
 * 
 * @author Koko jean olivier
 * @author Fabrice Tra
 *
 */
public class Backend
{
    /** Database.*/
    private InMemoryRestautantDataBase restaurant;

    public Backend()
    {
        restaurant = new InMemoryRestautantDataBase();
    }

    public Database getDatabase()
    {
        return restaurant;
    }

}
