
package org.inria.restlet.mta.backend;

public class Stand_cuisson {

    Boolean isCooking;


    public Stand_cuisson(){
        this.isCooking = false;
    }

    /**
     * Permet au cuisinier de cuire un repas
     * @throws InterruptedException {@link InterruptedException}
     */
   synchronized public void Cuire() throws InterruptedException {

       while(!isCooking){
            System.out.println("Cuisinier: J'attends un nouveau client");
            this.wait();
        }
            System.out.println("cuisson ok ");
            isCooking = false;
            this.notifyAll();
    }

    /**
     * Permet à un client de demander la cuisson de son repas
     * @throws InterruptedException {@link InterruptedException}
     */
    synchronized public void demanderCuisson() throws InterruptedException {

        while(isCooking){ 
            this.wait();
        }
            isCooking=true;
            this.notifyAll();
            
    }
}


