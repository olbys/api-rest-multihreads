package org.inria.restlet.mta.backend;

import java.util.Random;

import org.inria.restlet.mta.database.api.impl.InMemoryRestautantDataBase;

public class Client extends Thread {

	Random r;
	InMemoryRestautantDataBase restaurant;
	private int  identifiant;
	private String statut;
	public final String [] statut_list = {"WAITING_TO_ENTER","AT_THE_BUFFET","WAITING_THE_COOK","EATING","OUT"};

	/**
	 * Constructeur de la classe client
	 * @param id int
	 * @param resto {@link InMemoryRestautantDataBase}
	 */
	public Client(int id, InMemoryRestautantDataBase resto) {
		this.restaurant = resto;
		this.identifiant = id;
		r = new Random();
	}


	public int getIdentifiant() {
		return identifiant;
	}

	/**
	 * Retourne le niveau d'avancement du client dans la quête de son repas;
	 * @return String
	 */
	public String getStatut() {
		return statut;
	}

	/**
	 * Met à jour le status du client
	 * @param statut {@link String}
	 */
	private void setStatut(String statut) {
		this.statut = statut;
	}

	/**
	 * Permet au client de rentrer dans le restaurant
	 * @throws InterruptedException {@link  InterruptedException}
	 */
	public void entrer_dans_resto() throws InterruptedException {
		this.setStatut(statut_list[0]);
		restaurant.Entrer_restaurant();
	}

	/**
	 * Permet au client de se servir au buffet
	 * @throws InterruptedException {@link Exception}
	 */
	public void prendre_portions() throws InterruptedException {

		this.setStatut(statut_list[1]);
		// voici la formule utilisee pour calculer la portion prise par le client :
		// random = min + r.nextFloat() * (max - min);
		float randomPortion = r.nextFloat() * (100);
		restaurant.getBuffet().prendre_viande_cru(randomPortion);
		Thread.sleep(300);
		restaurant.getBuffet().prendre_poisson_cru(randomPortion);
		Thread.sleep(200);
		restaurant.getBuffet().prendre_legumes_cru(randomPortion);
		Thread.sleep(300);
		restaurant.getBuffet().prendre_nouilles_froides(randomPortion);
		Thread.sleep(300);
	}

	/**
	 * Permet au client de passer en cuisine et faire cuire son repas
	 * @throws InterruptedException {@link InterruptedException}
	 */
	public void passer_en_cuisine() throws InterruptedException {
		this.setStatut(statut_list[2]);
		restaurant.getStand_cuisson().demanderCuisson();
	}

	/**
	 * Permet au client de manger
	 * @throws InterruptedException {@link InterruptedException}
	 */
	public void manger() throws InterruptedException {
		this.setStatut(statut_list[3]);
		Thread.sleep(500);
	}

	/**
	 * Permet au client de s'en aller
	 * @throws InterruptedException
	 */
	public void sen_aller() throws InterruptedException {
		this.setStatut(statut_list[4]);
		restaurant.Sortir_restaurant();
	}



	/**
	 * Thread du client
	 */
	@Override
	public void run() {
		try {
			entrer_dans_resto();
			prendre_portions();
			passer_en_cuisine();
			manger();
			sen_aller();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}	

}
