package org.inria.restlet.mta.application;



import org.inria.restlet.mta.resources.BuffetResource;
import org.inria.restlet.mta.resources.ClientRessource;
import org.restlet.Application;
import org.restlet.Context;
import org.restlet.Restlet;
import org.restlet.routing.Router;

/**
 *
 * Application.
 *
 * @author msimonin
 *
 */
public class MyRestaurantApplication extends Application
{

    public MyRestaurantApplication(Context context)
    {
        super(context);
    }

    @Override
    public Restlet createInboundRoot()
    {
        Router router = new Router(getContext());
        router.attach("/buffet" , BuffetResource.class);
        router.attach("/client/{client_id}",ClientRessource.class);
        return router;
    }
}
