package org.inria.restlet.mta.resources;

import org.inria.restlet.mta.backend.Backend;
import org.inria.restlet.mta.backend.Buffet;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class BuffetResource extends ServerResource {

    /** Backend.*/
    private Backend backend_;

    /** Buffet géré par cette resource.*/
    private Buffet buffet;

    /**
     * Constructor.
     * Call for every single user request.
     */
    public BuffetResource(){
    backend_ = (Backend) getApplication().getContext().getAttributes()
            .get("backend");

    }

   @Get("json")
    public Representation getBuffet() throws Exception {
        buffet = backend_.getDatabase().getBuffet();
        if ( buffet == null){
            return new JsonRepresentation(new JSONObject().put("response","404 Buffet not found"));
        }
       JSONObject buffetObject = new JSONObject();
       buffetObject.put("WELCOME ", "BUFFET STATE");
       buffetObject.put("poisson_cru", buffet.getPoisson_cru());
       buffetObject.put("viande_cru", buffet.getViande_cru());
       buffetObject.put("legumes_cru", buffet.getLegumes_cru());
       buffetObject.put("nouilles_froides", buffet.getNouilles_froides());
       return new JsonRepresentation(buffetObject);
   }


}
