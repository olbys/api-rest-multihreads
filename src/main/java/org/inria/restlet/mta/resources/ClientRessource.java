package org.inria.restlet.mta.resources;

import org.inria.restlet.mta.backend.Backend;
import org.inria.restlet.mta.backend.Client;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class ClientRessource extends ServerResource {

    /** Backend.*/
    private Backend backend_;

    /** Utilisateur géré par cette resource.*/
    private Client client;

    /**
     * Constructor.
     * Call for every single user request.
     */
    public ClientRessource()
    {
        backend_ = (Backend) getApplication().getContext().getAttributes()
                .get("backend");
    }

    @Get("json")
    public Representation getClient() throws Exception {
        String clientId = (String) getRequest().getAttributes().get("client_id");
        int client_id = Integer.valueOf(clientId);
        client  = backend_.getDatabase().getClient(client_id);
        if( client == null){
            return new JsonRepresentation(new JSONObject().put("reponse","404 client not found"));
        }
        JSONObject clientObject = new JSONObject();
        clientObject.put("STATE", client.getStatut());
        clientObject.put("ID", client.getIdentifiant());
        return new JsonRepresentation(clientObject);
    }


}
