package partie_1;

import java.util.Random;

public class Client extends Thread {

	Random r;
	Restaurant restaurant;
	int id;
	enum statut {WAITING_TO_ENTER,AT_THE_BUFFET,WAITING_THE_COOK,EATING,OUT};


	public Client(int id, Restaurant restaurant) {
		this.restaurant = restaurant;
		this.id = id+1;
		r = new Random();
	}

	/**
	 * @throws InterruptedException
	 * 
	 */
	public void entrer_dans_resto() throws InterruptedException {
		System.out.println("client "+id+" Je rentre dans le restaurant, youpi !");
		restaurant.Entrer_restaurant();
	}

	public void prendre_portions() throws InterruptedException {
		// voici la formule utilisee pour calculer la portion prise par le client :
		// random = min + r.nextFloat() * (max - min);
		float randomPortion = r.nextFloat() * (100);
		restaurant.getBuffet().prendre_viande_cru(randomPortion);
		Thread.sleep(200);
		restaurant.getBuffet().prendre_poisson_cru(randomPortion);
		Thread.sleep(200);
		restaurant.getBuffet().prendre_legumes_cru(randomPortion);
		Thread.sleep(200);
		restaurant.getBuffet().prendre_nouilles_froides(randomPortion);
		Thread.sleep(200);
	}

	public void passer_en_cuisine() throws InterruptedException {
		System.out.println("client "+id+" Je passe en cuisine");
		restaurant.getStand_cuisson().demanderCuisson();
	}

	public void manger() throws InterruptedException {
		System.out.println("client "+id+" Je mange");
		Thread.sleep(300);
	}

	public void sen_aller() throws InterruptedException {
		System.out.println("client "+id+" Je sors du restaurant");
		restaurant.Sortir_restaurant();
	}

	@Override
	public void run() {
		try {
			entrer_dans_resto();
			prendre_portions();
			passer_en_cuisine();
			manger();
			sen_aller();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
