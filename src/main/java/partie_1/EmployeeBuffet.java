package partie_1;

public class EmployeeBuffet extends Thread {
	
	private Buffet buffet;
	private final float MIN_QUANTITY = 100;
	

	public EmployeeBuffet(Buffet buffet) {
		this.buffet = buffet;
		this.setDaemon(true);

	}
	

	@Override
	public void run() {
		System.out.println("l'employe de cuisson commence son service");
		this.parcourrir();
		System.out.println("fin du service employee");
	}


	public void parcourrir() {
		
		while(true) {
			if(this.buffet.getPoisson_cru()  < MIN_QUANTITY){
				System.out.println("Employee: Je rempli le bac de poisson cru");
				this.buffet.ajouter_poisson_cru();
			}

			if(this.buffet.getViande_cru() < MIN_QUANTITY){
				System.out.println("Employee: Je rempli le bac de viande crue");
				this.buffet.ajouter_viande_cru();
			}
				
			if(this.buffet.getLegumes_cru() < MIN_QUANTITY){
				System.out.println("Employee: Je rempli le bac de legumes");
				this.buffet.ajouter_legumes_cru();
			}

			if(this.buffet.getNouilles_froides() < MIN_QUANTITY){
				System.out.println("Employee: Je rempli le bac de nouilles");
				this.buffet.ajouter_nouilles_froides();
			}
		}
	}

}
