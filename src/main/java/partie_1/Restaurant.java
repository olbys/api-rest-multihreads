package partie_1;

import java.util.ArrayList;

public class Restaurant {

    private final static int LIMITED_CLIENT = 3 ;
    private ArrayList<Client> clientArrayList;
    private int nombre_client = 0 ; 
    private Buffet buffet ;
    private EmployeeBuffet employeeBuffet ;
    private Cuisinier cuisinier ;
    private Stand_cuisson stand_cuisson ;

    /**
     * Entrer dans un restaurant methode a appeler par un client lorsqu'il veut rentrer dans un restaurant
     * @throws InterruptedException
     */
    synchronized public void Entrer_restaurant() throws InterruptedException {
        while(nombre_client==LIMITED_CLIENT){
            System.out.println("pas de place pour l'instant, attends");
            this.wait();
        }
        //System.out.println("le client rentre dans le restaurant");
        augmenterClient();
    }

    /**
     * Sortir d'un restaurant methode a appeler par un client lorsqu'il veut rentrer dans un restaurant
     * @throws InterruptedException
     */
    synchronized public void Sortir_restaurant() throws InterruptedException {
        diminuerClient();
        //System.out.println("le client sort ");
        this.notifyAll();
    }

    /**
     * Lorsqu'il y a de la place et qu'un client reussir à rentrer on augmente le nombre de cient present a l'instant dans le restaurant
     */
    synchronized public void augmenterClient(){
        this.nombre_client ++ ;
    }

    /**
     * Lorsqu'un  qu'un client reussir sort du resto  on diminue  le nombre de cient present a l'instant dans le restaurant
     */
    synchronized public void diminuerClient(){
        this.nombre_client -- ;
    }

    /**
     * retoune le buffet du restaurant
     * @return Buffet
     */
    public Buffet getBuffet() {
        return buffet;
    }

    /**
     *
     * @param buffet {@link org.inria.restlet.mta.backend.Buffet}
     */
    public void setBuffet(Buffet buffet) {
        this.buffet = buffet;
    }

    /**
     * retourne l'adresse du stand cuisson du restaurant
     * @return Stand_cuisson {@link org.inria.restlet.mta.backend.Stand_cuisson}
     */
    public Stand_cuisson getStand_cuisson() {
        return stand_cuisson;
    }

    /**
     * @param stand_cuisson {@link org.inria.restlet.mta.backend.Stand_cuisson}
     */
    public void setStand_cuisson(Stand_cuisson stand_cuisson) {
        this.stand_cuisson = stand_cuisson;
    }


    /**
     * Constructor
     */
     public Restaurant(){
       
        //instanciation du buffet
        this.buffet = new Buffet();

        //instanciation de l'employe de buffet
        this.employeeBuffet = new EmployeeBuffet(buffet);
        this.employeeBuffet.start();

        //instanciation du stand de cuisson
        this.stand_cuisson = new Stand_cuisson();

        //instanciation du cuisinier
        this.cuisinier = new Cuisinier(stand_cuisson);
        this.cuisinier.start();

        //instanciation de la liste des clients
         this.clientArrayList = new ArrayList<Client>();

        //instanciation des clients
        for(int i = 0 ; i < 10 ; i++){
            clientArrayList.add(new Client(i+1,this));
            clientArrayList.get(i).start();
        }


    }


//    /**
//     * main de l'application partie 1
//     * @param args {@link String}
//     */
//    public static void main(String[] args) {
//        new Restaurant();
//    }



}