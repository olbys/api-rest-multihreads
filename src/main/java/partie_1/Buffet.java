package partie_1;


public class Buffet {

    private float poisson_cru;
    private float viande_cru;
    private float legumes_cru;
    private float nouilles_froides;
    
	private final  float MAX_QUANTITY = 1000;

	//BOOLEEN POUR S'ASSURER QUE L'EMPLOYE A L'EMPLOYE N'EST PAS DERANGE PAR SES CLIENTS QUAND IL FAIT SON TRAVAIL
	private Boolean isAddingViande = false;
	private Boolean isAddingPoisson = false;
	private Boolean isAddingLegumes = false;
	private Boolean isAddingNouilles = false;


    public Buffet(){
        poisson_cru = MAX_QUANTITY;
        viande_cru = MAX_QUANTITY;
        legumes_cru = MAX_QUANTITY;
        nouilles_froides = MAX_QUANTITY;  
	}

    // POISSON 
	public float getPoisson_cru() {
		return poisson_cru;
	}
	
	synchronized public void prendre_poisson_cru(float quantite) throws InterruptedException {
	
		while (getPoisson_cru() < quantite && isAddingPoisson) {
			this.wait();
		}
		System.out.println("Le client prends une portion de poisson cru");
		this.poisson_cru -= quantite;
		
	}
	synchronized public void ajouter_poisson_cru() {

		isAddingPoisson = true;
		this.poisson_cru = MAX_QUANTITY;
		isAddingPoisson = false;
		System.out.println("bac de poisson cru rempli");
		this.notifyAll();
	}
	
	//

	// VIANDES
	public float getViande_cru() {
		return viande_cru;
	}
	synchronized public void prendre_viande_cru(float quantite) throws InterruptedException {
		while (getViande_cru() < quantite && isAddingViande) {
			wait();
		}
		System.out.println("Le client prends une portion de viande crue");

		this.viande_cru -= quantite;
	}
	synchronized public void ajouter_viande_cru() {
		isAddingViande = true;
		this.viande_cru = MAX_QUANTITY;
		isAddingViande = false;
		System.out.println("bac de poisson cru rempli");
		this.notifyAll();
	}
	// 



	// LEGUMES 
	
	public float getLegumes_cru() {
		return legumes_cru;
	}

	synchronized public void prendre_legumes_cru(float quantite) throws InterruptedException {
		while (getLegumes_cru() < quantite && isAddingLegumes) {
			wait();
		}
		System.out.println("Le client prends une portion de legumes");

		this.legumes_cru -= quantite;
	}
	
	synchronized public void ajouter_legumes_cru(){
		isAddingLegumes = true;
		this.legumes_cru = MAX_QUANTITY;
		isAddingLegumes = false;
		System.out.println("bac de poisson cru rempli");
		this.notifyAll();
	}
	//

	// NOUILLES

	public float getNouilles_froides() {
		return nouilles_froides;
	}
	
	/**
	 * 
	 * @param quantite
	 * @throws InterruptedException
	 */
	public void prendre_nouilles_froides(float quantite) throws InterruptedException {
		
		while (getNouilles_froides() < quantite && isAddingNouilles) {
			wait();
		}
		System.out.println("Le client prends une portion de nouilles froides");
		this.nouilles_froides -= quantite;
	}

	synchronized public void ajouter_nouilles_froides() {
		isAddingNouilles = true;
		this.nouilles_froides = MAX_QUANTITY;
		isAddingNouilles = false;
		System.out.println("bac de poisson cru rempli");
		this.notifyAll();
	}
	//

    
    











}
