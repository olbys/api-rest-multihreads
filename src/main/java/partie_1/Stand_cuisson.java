
package partie_1;

public class Stand_cuisson {

    Boolean isCooking;


    public Stand_cuisson(){
        this.isCooking = false;
    }

   synchronized public void Cuire() throws InterruptedException {

       while(!isCooking){
            System.out.println("Cuisinier: J'attends un nouveau client");
            this.wait();
        }
            System.out.println("cuisson ok ");
            isCooking = false;
            this.notifyAll();
    }

    synchronized public void demanderCuisson() throws InterruptedException {

        while(isCooking){ 
            System.out.println("Cuisinier: Une cuisson est en cours, patiente stp");
            this.wait();
        }
            //System.out.println("isCooking = true");
            isCooking=true;
            this.notifyAll();
            
    }
}


