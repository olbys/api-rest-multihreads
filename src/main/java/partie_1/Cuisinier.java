package partie_1;

public class Cuisinier extends Thread {
    Stand_cuisson stand;

    public Cuisinier(Stand_cuisson stand) {
        this.stand = stand;
        this.setDaemon(true);

    }
    
    private void Commencer_service(){
        System.out.println("le cuisinier commence son service");
        while (true){
            try {
                this.stand.Cuire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * 
     */
    @Override
    public void run() {
        Commencer_service();
        System.out.println("le cuisinier a son finir");
	}

}